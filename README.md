Basic generator webapp for f2e
=======

the basic solution for f2e - base yeoman solution plus

## Demo
[Demo]


## Installation
1. default deploy

  		npm --version 1.2.11

  		grunt-cli v0.1.9

  		grunt v0.4.1

2. install npm

		npm install

3. install bower

		bower install



## Setup
* [How to setup on your website]

## support
* html5/html
* sass,scss
* coffeeScript
* jquery
* template
* json data


## Bower Components
* sass-bootstrap: "~2.3.0"
* requirejs: "~2.1.4"
* modernizr: "~2.6.2"
* jquery: "~1.9.1"


## NPM Components - Template
* grunt-contrib-handlebars: "~0.5.9"


## Data
* json type


## Browsers
* Google Chrome
* Apple Safari 4.0+
* Mozilla Firefox 3.0+
* Opera 11.0+
* Microsoft Internet Explorer 6.0+