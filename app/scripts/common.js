define([], function() {


  return {

    methodName: function () {
        console.log("mmm");
        function cut () {
            console.log("cut");
        };
        function cuta () {
            console.log("cuta");
        };
        cuta();

        (function($){
            console.log('fun01');
        })(jQuery);

        (function() {
            console.log('fun02');
        })();

        (function hoge() {
            console.log('fun03');
        })();

    },

    limitBoardContent: function(){

        // ### limit content string ###
        // obj     : target id & class
        // overStr : over default string num can be excute
        // limitStr: display limit string num
        // addLast : add last string

        function limitContent (obj,overStr,limitStr,addLast) {
            $(obj).each(function () {

                var selfDiv = $(this),
                    getContent = selfDiv.text();

                if (getContent.length > overStr) {
                    selfDiv.text(getContent.substr(0,limitStr));
                    selfDiv.append(addLast);
                }
            });
        }
        limitContent('.limitContent',60,128,'...'); // board content

    },


    backToTop: function() {
          var anchor = $('<a>', {'id': 'back_to_top_btn', 'class':'back_to_top', 'href':'#logo', 'title': 'Back to Top', 'text': 'Top'});
          anchor.appendTo($(document.body)).hide();

          $(window).scroll(function(event){
              if(getScrollTop() > 150) {
                  anchor.fadeIn(300);
              } else {
                  anchor.fadeOut(300, function() {$(this).hide();});
              }
          });

          anchor.click(function(event) {
              event.preventDefault();
              var elem = getScrollableElement('html', 'body');
              $(elem).animate({scrollTop: 0}, 400);
          });

        function getScrollableElement(element) {
            for (var b = 0, c = arguments.length; b < c; b++) {
            var d = arguments[b]
            var e = $(d);
            if (e.scrollTop() > 0) return d;
            else {
            e.scrollTop(1);
            var f = e.scrollTop() > 0;
            e.scrollTop(0);
            if (f) return d
            }
        }
        return []
        }


        function getScrollTop() {
            var top = 0;
            top = top !== 0 ? top : (window.pageYOffset ? window.pageYOffset : 0);
            top = top !== 0 ? top : (document.documentElement ? document.documentElement.scrollTop : 0);
            top = top !== 0 ? top : (document.body ? document.body.scrollTop : 0);
            return top;

            }
    }};
});