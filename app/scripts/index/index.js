jQuery(document).ready(function() {

    var jdata = null;

    $.ajax({
        url: 'tpl/_data/login.json',
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {jdata = data; }
    });

    var tpl = HBS['main'](jdata);
    $('#tpl-joinNow').html(tpl);

    var tpl = HBS['welcome'](jdata);
    $('#tpl-welcome').html(tpl);

});



