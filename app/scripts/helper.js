define([], function() {

  return {
    helper: function () {
      Handlebars.registerHelper('html_safe', function(text) {
          return new Handlebars.SafeString(text);
      });
    },
    debug: function () {
      Handlebars.registerHelper("debug", function(text) {
        console.log("Current Context");
        console.log("====================");
        console.log(this);

        if (text) {
          console.log("Value");
          console.log("====================");
          console.log(text);
        }
      });
    },
    limit_ary: function () {
      Handlebars.registerHelper("each_upto", function(ary, max, options) {
          if(!ary || ary.length == 0)
              return options.inverse(this);
          var result = [];
          for(var i = 0; i < max && i < ary.length; ++i)
              result.push(options.fn(ary[i]));
          return result.join('');
      });
    }};

});
